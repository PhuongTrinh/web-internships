@extends('user.user')
@section('content')
<!-- all product -->
    <div class="page">
        <div class="container">
            <p>Home/product</p>
        </div>
    </div>
    <div class="Title my-3">
        <h1>All Product</h1>
    </div>
    <section class="allProduct" id="AllProduct" >
        <div class="container">
            <!-- pagination -->
           <div style="">
                <nav aria-label="Page navigation example" >
                    <ul class="pagination">
                    </ul>
                </nav>
           </div>
            <div class="row" id="allCar">
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-1.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-1.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-primary">View more</button>
                                <button class="btn btn-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- unknow -->
    <section class="unknown">
        <div class="container-fluid">
            <div class="row" >
                <div class="col-4 left">
                    <img src="image/car/unkown.png" alt="">
                </div>
                <div class="col-8 right">
                    <div class="text">
                        <h3>Our products are friendly to every family </h3>
                        <p>HONDA Safe and durable</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- top cars -->
    <section id="topCar">
        <div class="container">
            <div class="Title my-3">
                <h1>Top Cars</h1>
            </div>
            <!-- owlcarousel -->
            <div class="owl-carousel owl-theme" id="topCars">
                <div class="item" >
                    <div class="row rowItem">
                        <div class="col-5 left">
                            <img src="image/car/topCar1.jpg" alt="">
                        </div>
                        <div class="col-7 right">
                            <div class="contentCar">
                                <span>30.12.2018</span>
                                <h2>Lorem ipsum dolor sit amet consectetur adipisicing.</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit nisi consequatur doloribus reiciendis quam tempore.</p>
                                <p>
                                    <a href="#">
                                        <span>Detail </span>
                                        <span><i class="fa fa-angle-double-right"></i></span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item" >
                     <div class="row rowItem" >
                        <div class="col-5 left">
                            <img src="image/car/topCar2.jpg" alt="">
                        </div>
                        <div class="col-7 right">
                            <div class="contentCar">
                                    <span>30.12.2018</span>
                                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing.</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit nisi consequatur doloribus reiciendis quam tempore.</p>
                                    <p>
                                        <a href="#">
                                            <span>Detail </span>
                                            <span><i class="fa fa-angle-double-right"></i></span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="item">
                     <div class="row rowItem">
                        <div class="col-5 left">
                            <img src="image/car/topCar3.jpg" alt="">
                        </div>
                        <div class="col-7 right">
                            <div class="contentCar">
                                    <span>30.12.2018</span>
                                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing.</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit nisi consequatur doloribus reiciendis quam tempore.</p>
                                    <p>
                                        <a href="#">
                                            <span>Detail </span>
                                            <span><i class="fa fa-angle-double-right"></i></span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
               
            </div>
        </div>
    </section>

        <!-- new car -->
        <section id="newCars">
            <div class="container">
                <div class="Title my-3">
                    <h1>New Cars</h1>
                </div>
                <div class="row">
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 img">
                        <div class="card cardContent">
                            <div class="imghover">
                                <img src="image/car/topCar3.jpg" alt="">
                            </div>
                            <div class="card-body cardBody">
                                <p>HONDA JAZZ</p>
                                <div class="codePrice">
                                    <p>code: 111</p>
                                    <p>price: $999</p>
                                </div>
                            </div>
                            <div class="viewMore">
                                <div class="text">
                                    <p>HONDA JAZZ</p>
                                    <button class="btn btn-outline-warning">View more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- accessoriess -->
        <section id="accessories">
            <div class=" contentFluid">
                <div class="Title my-3">
                    <h1>Service</h1>
                    <p>We provide the best service to customers</p>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                    <img src="image/car/icon1.png" alt="">
                                    <img src="image/car/service1.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Kiểm ta trước khi giao xe</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                <img src="image/car/icon2.png" alt="">
                                    <img src="image/car/service2.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Bảo hành sản phẩm</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                <img src="image/car/icon.png" alt="">
                                    <img src="image/car/service3.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Dịch vụ bảo dưỡng miễn phí</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                    <img src="image/car/icon3.png" alt="">
                                    <img src="image/car/service4.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Tư vấn sử dụng</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                <img src="image/car/icon4.png" alt="">
                                    <img src="image/car/service5.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>FAQ</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left ">
                                <img src="image/car/icon5.png" alt="">
                                    <img src="image/car/service6.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Kĩ thuật công nghệ</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                <img src="image/car/icon6.png" alt="">
                                    <img src="image/car/service7.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Đào tạo kĩ thuật viên</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pr-0">
                            <div class="row sericeItem">
                                <div class="col-3 left">
                                <img src="image/car/icon7.png" alt="">
                                    <img src="image/car/service8.png" alt="">
                                </div>
                                <div class="col-9 right">
                                    <p>Đăng kí bảo dưỡng online</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    <!-- end -->
@endsection