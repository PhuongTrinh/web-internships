<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function Index(){
    	return view('user.layouts.index');
    }

    public function Product(){
    	return view('user.layouts.product');
    }

	public function Login(){
    	return view('user.userlogin');
    }

}
